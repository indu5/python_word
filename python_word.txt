
Hangman game_code_Python


  import logging

  logging.basicConfig(level=logging.DEBUG)
  import random



  #making the list

  words=['computer','mobile','laptop']



  #list of letters of the word

  letters=[]



  #selecting random word from the list

  word1=(random.choice(words))



  #printing a random word

  logging.debug(word1)



  #display each letter of the word with "_" and assign it to variable display

  display='_'*len(word1)



  #make a list of wrong guesses

  wrong_list=[]



  #the length of the word is assigned to the variable turns

  turns=len(word1)



  #creating a while loop

  while turns<10:
   
  
      #user's guess can be assigned to the variable 'guess'
    
      guess=input("Enter your guess:")
  
  
   
      #if the guesses letter is in the given word, append the letter in letters.
    
      if guess in word1:
        
          letters.append(guess)
        
          logging.debug("Your guess is correct")
  
      
       
          #if the positon of the letter in word is found,then print that letter in the exact position and remaining with '_'
        
          i= word1.find(guess)
        
          display=display[:i] +guess +display[i+1:]
        
          print("the word is:",display)
        
          if len(letters)==len(word1):
            
              loggong.debug("You have won")
            
              break

        
    
      #if the user entered the wrong letter, then print 'your guess is wrong'    
    
      else:
 
       
          #the wrong guesses are appended to the wrong_list
        
          wrong_list.append(guess)
        
          print("Not lucky! Your guess is wrong")
  
      
    
          #if the user makes more than expected wrong guesses, print the game is over
    
      if len(wrong_list)>6:
        
          print("The game is over")
        
          break
        
        

